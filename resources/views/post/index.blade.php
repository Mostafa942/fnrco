@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- start tabs --}}
        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="my-posts" data-url="{{ route('get-posts') }}" data-bs-toggle="tab"
                    data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">My
                    Posts</button>
            </li>
        </ul>
        {{-- end tabs --}}
        <div class="row mt-3">
            <div class="col-12">
                <button type="button" id="btn-add" data-url="{{ route('post.create') }}" class="btn btn-success">Add
                    {{ !count($posts) ? 'your first' : '' }} post</button>
            </div>
        </div>

        {{-- start content --}}
        <div id="my-content">
            @include('post.posts', ['posts' => $posts])
        </div>
    </div>
@endsection
@section('js')
    <script>
        let page = 1;
        $(document).on('click', '#my-posts', function() {
            getPosts(1, $(this).data('url'), 'empty');
            paginate = true;
        })

        function getPosts(page = 1, url, flag) {
            $.ajax({
                type: 'GET',
                url: url + '?page=' + page,
                success: function(res) {
                    if (flag == 'empty') {
                        $('#my-content').empty();
                        $('#my-content').html(res.data);
                    } else {
                        $('#my-content').append(res.data);
                    }
                }
            })
        }

        $(window).scroll(function() {
            if ($(window).scrollTop() + $(window).height() == $(document).height() && paginate) {
                page = ++page;
                getPosts(page, '{{ route('get-posts') }}', 'append');
            }
        });
    </script>
@endsection
