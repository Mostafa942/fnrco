<section class="hero">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 offset-lg-3">

                <div class="cardbox shadow-lg bg-white">

                    <div class="cardbox-heading">
                        <!-- START dropdown-->
                        <div class="dropdown float-right">
                            <div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu"
                                style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item" href="#">Hide post</a>
                                <a class="dropdown-item" href="#">Stop following</a>
                                <a class="dropdown-item" href="#">Report</a>
                            </div>
                        </div>
                        <!--/ dropdown -->
                        <div class="media m-0">
                            <div class="d-flex mr-3">
                                <button
                                    class="rounded-circle btn btn-sm btn-dark">{{ substr($post->user->name, 0, 1) }}</button>
                            </div>
                            <div class="media-body">
                                <p class="m-0">{{ $post->user->name }}</p>
                                {{-- <small><span><i class="icon ion-md-pin"></i> Nairobi, Kenya</span></small> --}}
                                <small><span><i class="icon ion-md-time"></i> {{ $post->time }}</span></small>
                            </div>
                            @if (auth()->user()->id == $post->user->id)
                                <div>
                                    <form action="{{ route('post.destroy', $post->id) }}" method="POST"
                                        id="form-delete">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger">Delete</button>

                                    </form>
                                </div>
                            @endif

                        </div>

                        <!--/ media -->
                    </div>
                    @if (count($post->tags))
                        <div class="cardbox-body">
                            <ul class="">
                                @foreach ($post->tags as $tag)
                                    <li class="border-0" style="display:inline">
                                        <button data-href="{{ route('hashtag-posts', $tag->id) }}" class="btn btn-link p-0 hash-tag">#{{ $tag->tag }}</button>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (count($post->files))
                        <!--/ cardbox-heading -->
                        <div class="cardbox-item">
                            <img class="img-fluid" src="/storage/{{ $post->files->first()->path }}" alt="Image">
                        </div>
                    @endif
                    {{-- cardbox body --}}
                    <div class="cardbox-body">
                        <h2>{{ $post->title }}</h2>
                        <p>{{ $post->description }}</p>
                    </div>
                    {{-- cardbox body --}}
                    <!--/ cardbox-item -->
                    <div class="cardbox-base">
                        <ul>
                            {{-- <li><a><i class="fa fa-thumbs-up"></i></a></li> --}}
                            @foreach ($post->LikersAvatar as $item)
                                <li>
                                    <button class="rounded-circle btn btn-sm btn-danger">{{ $item }}</button>
                                </li>
                            @endforeach
                            <li><a><span id="like-count">{{ count($post->likes) }} Likes</span></a></li>
                        </ul>
                        <ul class="float-right">
                            <li><a><i class="fa fa-comments">Comments</i></a></li>
                            <li><a><em class="mr-5 comments-count">{{ count($post->comments) }}</em></a></li>
                        </ul>
                        <table class="table table-borderd mb-0 text-center">
                            <tr>
                                <td>
                                    <button type="button"
                                        class="btn like {{ $post->likes()->where('user_id', auth()->user()->id)->first()? 'btn-primary': '' }}"
                                        id="like" data-url="{{ route('like-post', $post->id) }}">Like</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ cardbox-base -->
                    <div class="cardbox-body pb-0" id="comments" style="overflow-y: scroll; max-height:100px;">
                        @foreach ($post->comments as $comment)
                            <p>{{ $comment->comment }}</p>
                        @endforeach
                    </div>
                    <div class="cardbox-comments mt-5">
                        {{-- <span class="comment-avatar float-left">
                            <a href=""><img class="rounded-circle"
                                    src="http://www.themashabrand.com/templates/bootsnipp/post/assets/img/users/6.jpg"
                                    alt="..."></a>
                        </span> --}}
                        <form action="{{ route('store-comment', $post->id) }}" id="form-comment" method="POST"
                            class="">
                            @csrf
                            <div class="search">
                                <input placeholder="Write a comment" name="comment" type="text">
                                <button type="submit" class="btn btn-primary">comment</button>
                            </div>
                        </form>
                        <!--/. Search -->
                    </div>
                    <!--/ cardbox-like -->

                </div>
                <!--/ cardbox -->

            </div>
            <!--/ col-lg-6 -->
            <!--/ col-lg-3 -->

        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>
