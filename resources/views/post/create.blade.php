<div class="row justify-content-center mt-2">
    <div class="col-8">
        <div class="form-group">
            <form action="{{ route('post.store') }}" method="POST" id="store-post" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <p>Title:</p>
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                <div class="mb-3">
                    <p>Description:</p>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="10"></textarea>
                </div>
                <div id="hash-tag">
                    <div class="row" id="1">
                        <div class="col-10">
                            <div class="mb-3">
                                <p>Hash Tags:</p>
                                <input type="text" name="hashTags[1]" id="" class="form-control">
                            </div>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-danger btn-delete" data-row-id="1">Remove</button>
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <button type="button" class="btn btn-info" id="btn-add-hash">Add</button>
                </div>
                <div class="mb-3">
                    <p>Photo</p>
                    <input type="file" name="file" id="file">
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Post</button>
                </div>
            </form>
        </div>
    </div>
</div>
