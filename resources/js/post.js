const toastr = require("toastr");
paginate = true;
$(document).on('click', '#btn-add', function () {
    let url = $(this).data('url');
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#my-content').empty()
            $('#my-content').html(res.data);
            paginate = false;
        }
    })
});

$(document).on('submit', '#store-post', function (e) {
    e.preventDefault();
    let url = $(this).attr('action'),
        data = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        success: function (res) {
            page = 1;
            $('#my-content').empty();
            $('#my-content').html(res.data);
            paginate = false;
        },
        error: function (res) {
            let errors = res.responseJSON.errors;
            for (const error in errors) {
                toastr.error("Error! " + errors[error]);
            }
        }
    })
})

$(document).on('click', '.like', function (e) {
    e.preventDefault();
    let url = $(this).data('url');
    let btn = $(this);
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            if (res.message == 'dislike') {
                btn.removeClass('btn-primary');
            } else {
                btn.addClass('btn-primary');
            }
            btn.parent().parent().parent().parent().parent().find('#like-count').text(res.data + ' Likes');
        }
    })
})

$(document).on('submit', '#form-comment', function (e) {
    e.preventDefault();
    let url = $(this).attr('action'),
        data = $(this).serialize(),
        form = $(this);
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (res) {
            let commentCount = form.parent().parent().parent().find('.comments-count').text();
            parseInt(commentCount);
            form.parent().parent().parent().find('.comments-count').text(++commentCount);
            form.parent().parent().find('#comments').append(
                `<p>${res.data.comment}</p>`
            )
        },
        error: function (res) {
            let errors = res.responseJSON.errors;
            for (const error in errors) {
                toastr.error("Error! " + errors[error]);
            }
        }
    })
})

$(document).on('click', '#btn-add-hash', function () {
    let rowId = Date.now();
    console.log(rowId)
    let row = `<div class="row" id="${rowId}">
                    <div class="col-10">
                        <div class="mb-3">
                            <p>Hash Tags:</p>
                            <input type="text" name="hashTags[${rowId}]" id="" class="form-control">
                        </div>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-danger btn-delete" data-row-id="${rowId}">Remove</button>
                    </div>
                </div>
                `
    $('#hash-tag').append(row);
    paginate = false;
})

$(document).on('click', '.btn-delete', function () {
    $('#' + $(this).data('row-id')).remove();
})

$(document).on('click', '.hash-tag', function (e) {
    e.preventDefault();
    let url = $(this).data('href');
    $.ajax({
        type: 'GET',
        url: url,
        success: function (res) {
            $('#my-content').empty()
            $('#my-content').html(res.data);
            paginate = false;
        }
    })
})
