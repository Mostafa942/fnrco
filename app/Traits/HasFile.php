<?php

namespace App\Traits;

use App\Models\File;

trait HasFile
{
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
