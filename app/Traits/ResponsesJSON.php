<?php

namespace App\Traits;

trait ResponsesJSON
{
    protected function json($data = null, $message = null, $headerCode = 200, $status = true)
    {
        return response()->json([
            'status' => $status,
            'message' => $message ?? trans('lang.success'),
            'data' => $data
        ], $headerCode);
    }

    protected function validationError($data = null, $message = null, $headerCode = 200, $status = false)
    {
        return response()->json([
            'status' => $status,
            'message' => $message ?? trans('lang.validation_error'),
            'data' => $data
        ], $headerCode);
    }
}
