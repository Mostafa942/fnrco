<?php

namespace App\Services;

use App\Http\Repositories\PostRepositoryInterface;

class PostService
{
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        return $this->postRepository->index();
    }

    public function store($data)
    {
        return $this->postRepository->create($data);
    }

    public function likePost($post)
    {
        return $this->postRepository->likePost($post);
    }

    // public function hashTagsPosts($tag)
    // {
    //     return $this->postRepository->hashTagsPosts($tag);
    // }
}
