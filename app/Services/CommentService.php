<?php

namespace App\Services;

use App\Http\Repositories\CommentRepositoryInterface;

class CommentService
{
    protected $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function store($data, $post)
    {
        return $this->commentRepository->create($data, $post);
    }
}
