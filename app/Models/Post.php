<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\User;
use App\Models\HashTag;
use App\Traits\HasFile;
use App\Traits\HasComment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;
    use HasFile;
    use HasComment;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'post_likes', 'post_id', 'user_id');
    }

    public function getTimeAttribute()
    {
        if (Carbon::parse($this->created_at)->diffInSeconds() < 60) {
            $x = Carbon::parse($this->created_at)->diffInSeconds();
            return $x = 'Just now';
        } elseif (Carbon::parse($this->created_at)->diffInMinutes() < 60) {
            return Carbon::parse($this->created_at)->diffInMinutes() . ' minutes ago';
        } elseif (Carbon::parse($this->created_at)->diffInMinutes() < 720) {
            return Carbon::parse($this->created_at)->diffInHours() . ' hours ago';
        } elseif (Carbon::parse($this->created_at)->diffInMinutes() > 720 && Carbon::parse($this->created_at)->diffInMinutes() < 1440) {
            $x = Carbon::parse($this->created_at)->diffInMinutes();
            return $x = 'Today';
        } else {
            return Carbon::parse($this->created_at)->diffInDays() . ' days ago';
        }
    }

    public function getLikersAvatarAttribute()
    {
        $likersAvatar = [];
        $likers = $this->likes()->limit(4)->select('name')->get();
        foreach ($likers as $liker) {
            $likersAvatar[] = substr($liker->name, 0, 1);
        }
        return $likersAvatar;
    }

    public function tags()
    {
        return $this->belongsToMany(HashTag::class, 'post_hash', 'post_id', 'hash_tag_id');
    }
}
