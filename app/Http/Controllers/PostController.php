<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\HashTag;
use App\Services\PostService;
use App\Traits\ResponsesJSON;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

class PostController extends Controller
{
    use ResponsesJSON;

    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->postService->index();
        return view('post.index', ['posts' => $posts]);
    }

    public function getPostsAjax()
    {
        $posts = $this->postService->index();
        return $this->json(view('post.posts', ['posts' => $posts])->toHtml());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = view('post.create')->toHtml();
        return $this->json($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $data = $request->all();
        $post = $this->postService->store($data);
        return $this->json(view('post.single_post', ['post' => $post])->toHtml());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (auth()->user()->id == $post->user_id) {
            $post->delete();
        }
        return redirect(route('post.index'));
    }

    public function likePost(Post $post)
    {
        $like = $this->postService->likePost($post);
        return $this->json(count($post->likes), $like);
    }

    public function hashTagsPosts(HashTag $tag)
    {
        $posts = $tag->posts;
        return $this->json(view('post.posts', ['posts' => $posts])->toHtml());
    }
}
