<?php

namespace App\Http\Repositories;

use Illuminate\Contracts\Container\Container;

interface CommentRepositoryInterface
{
    public function create($data, $post);
}
