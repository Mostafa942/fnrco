<?php

namespace App\Http\Repositories;

use Illuminate\Contracts\Container\Container;

interface PostRepositoryInterface
{
    public function index();

    public function create($data);

    public function likePost($post);

    // public function hashTagsPosts($tag);
}
