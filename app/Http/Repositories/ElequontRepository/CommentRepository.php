<?php

namespace App\Http\Repositories\ElequontRepository;

use App\Http\Repositories\CommentRepositoryInterface;
use App\Models\Comment;

class CommentRepository implements CommentRepositoryInterface
{
    protected $model;

    public function __construct(Comment $model)
    {
        $this->model = $model;
    }

    public function create($data, $post)
    {
        $comment = $post->comments()->create([
            'user_id' => auth()->user()->id,
            'comment' => $data['comment']
        ]);
        return $comment;
    }
}
