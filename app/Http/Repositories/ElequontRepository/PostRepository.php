<?php

namespace App\Http\Repositories\ElequontRepository;

use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use App\Http\Repositories\PostRepositoryInterface;
use App\Models\HashTag;
use Illuminate\Support\Facades\DB;

class PostRepository implements PostRepositoryInterface
{
    protected $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        return auth()->user()->posts()->with([
            'user',
            'likes',
            'files',
            'comments'
        ])
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
    }

    public function create($data)
    {
        $post = DB::transaction(function () use ($data) {
            $post = $this->model->create([
                'title' => $data['title'],
                'description' => $data['description'],
                'user_id' => auth()->user()->id
            ]);
            if (isset($data['file'])) {
                $file = $data['file'];
                $filename = time() . pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

                Storage::disk('public')->putFileAs(
                    'files',
                    $file,
                    $filename
                );

                $upload = $post->files()->create([
                    'name' => $filename,
                    'path' => 'files/' . $filename,
                    'type' => $file->getClientMimeType(),
                ]);
            }
            if (isset($data['hashTags'])) {
                foreach ($data['hashTags'] as $hashTag) {
                    $newHashTag = HashTag::firstOrCreate([
                        'tag' => $hashTag
                    ]);
                    $post->tags()->attach($newHashTag->id);
                }
            }
            return $post->load(
                'user',
                'likes',
                'files',
                'comments'
            );
        });
        return $post;
    }

    public function likePost($post)
    {
        $like = $post->likes()->where('user_id', auth()->user()->id)->first();
        if ($like) {
            $post->likes()->detach(auth()->user()->id);
            return $message = 'dislike';
        }
        $post->likes()->attach(auth()->user()->id);
        return $message = 'like';
    }

    // public function hashTagsPosts($tag)
    // {
    //     $tag->posts;
    // }
}
