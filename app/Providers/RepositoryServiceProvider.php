<?php

namespace App\Providers;

use App\Http\Repositories\CommentRepositoryInterface;
use App\Http\Repositories\ElequontRepository\CommentRepository;
use App\Http\Repositories\ElequontRepository\PostRepository;
use App\Http\Repositories\PostRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostRepositoryInterface::class, PostRepository::class);
        $this->app->bind(CommentRepositoryInterface::class, CommentRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
