<?php

use App\Http\Controllers\CommentContrroler;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('user')->middleware('auth')->group(function () {
    Route::resource('post', PostController::class);
    Route::get('/like/{post}', [PostController::class, 'likePost'])->name('like-post');
    Route::get('/get-posts', [PostController::class, 'getPostsAjax'])->name('get-posts');
    Route::post('/store-comment/{post}', [CommentContrroler::class, 'store'])->name('store-comment');
    Route::get('hashtag-posts/{tag}', [PostController::class, 'HashTagsPosts'])->name('hashtag-posts');
});
