## Posts

This application for share posts.

## Application  Setup

### Prerequisites
You have to have docker installed

### Initialization Steps (Enter the following commands in the command prompt)
1) Install the required docker containers

    `docker-compose up -d php-fpm workspace phpmyadmin`

2) Use zsh in the workspace conainer

    `docker-compose exec --user=laradock workspace zsh`

3) Navigate into the project directory

    `cd app`

4) Run composer install to install the required packages

    `composer install`

5) Run mpm install to install the required packages

    `npm install`

6) Run npm development

    `npm run dev`




### How to use the application
1) Register with the email and the password.
2) Enter the first post for you.
3) Scroll down to see your old posts. 
